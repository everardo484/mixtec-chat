package android.support.v4.app;

public final class BackstackAccessor {

    private BackstackAccessor() {
        throw new IllegalStateException("Not instantiatable");
    }

    public static boolean isFragmentOnBackStack(Fragment fragment) {
        return fragment.isInBackStack();
    }
}
