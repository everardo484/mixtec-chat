package org.mixteco.mixtecchat.common;

import android.os.Bundle;

import org.mixteco.mixtecchat.MixtecChatApp;
import org.mixteco.mixtecchat.di.ApplicationComponent;
import org.mixteco.mixtecchat.mvp.MvpFragment;
import org.mixteco.mixtecchat.mvp.MvpPresenter;
import org.mixteco.mixtecchat.mvp.MvpView;

public abstract class BaseFragment<V extends MvpView, P extends MvpPresenter<V>>  extends MvpFragment<V, P> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies(MixtecChatApp.graph);
    }

    protected abstract void injectDependencies(ApplicationComponent applicationComponent);
}
