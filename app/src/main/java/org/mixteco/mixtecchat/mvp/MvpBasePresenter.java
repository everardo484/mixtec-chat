package org.mixteco.mixtecchat.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.UiThread;

import java.lang.ref.WeakReference;

public class MvpBasePresenter<V extends MvpView> implements MvpPresenter<V> {

    public interface ViewAction<V> {
        void run(@NonNull V view);
    }

    private WeakReference<V> viewRef;
    private boolean presenterDestroyed = false;

    @UiThread
    @Override
    public void attachView(V view) {
        viewRef = new WeakReference<V>(view);
        presenterDestroyed = false;
    }

    protected final void ifViewAttached(ViewAction<V> action) {
        final V view = viewRef == null ? null : viewRef.get();
        if (view != null) {
            action.run(view);
        }
    }

    @Override
    public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }

    @Override
    public void destroyPresenter() {
        presenterDestroyed = true;
    }
}
