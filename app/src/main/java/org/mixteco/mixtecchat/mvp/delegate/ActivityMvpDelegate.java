package org.mixteco.mixtecchat.mvp.delegate;

import android.os.Bundle;

import org.mixteco.mixtecchat.mvp.MvpPresenter;
import org.mixteco.mixtecchat.mvp.MvpView;

public interface ActivityMvpDelegate<V extends MvpView, P extends MvpPresenter<V>> {

    void onCreate(Bundle bundle);

    void onDestroy();

    void onPause();

    void onResume();

    void onStart();

    void onStop();

    void onRestart();

    void onContentChanged();

    void onSaveInstanceState(Bundle outState);

    void onPostCreate(Bundle savedInstanceState);

}
