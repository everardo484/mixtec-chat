package org.mixteco.mixtecchat.mvp.delegate;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import org.mixteco.mixtecchat.mvp.MvpPresenter;
import org.mixteco.mixtecchat.mvp.MvpView;

public interface FragmentMvpDelegate<V extends MvpView, P extends MvpPresenter<V>> {

    void onCreate(Bundle saved);

    void onDestroy();

    void onViewCreated(View view, @Nullable Bundle savedInstanceState);

    void onDestroyView();

    void onPause();

    void onResume();

    void onStart();

    void onStop();

    void onActivityCreated(Bundle savedInstanceState);

    void onAttach(Activity activity);

    void onDetach();

    void onSaveInstanceState(Bundle outState);
}
