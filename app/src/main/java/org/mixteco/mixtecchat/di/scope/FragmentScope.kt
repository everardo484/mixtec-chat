package org.mixteco.mixtecchat.di.scope

import javax.inject.Scope

@Scope
annotation class FragmentScope