package org.mixteco.mixtecchat.di

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecchat.di.scope.FragmentScope

@Module
abstract class FragmentModule(protected val fragment: Fragment) {

    @Provides
    @FragmentScope
    fun provideFragment(): Fragment = fragment

}