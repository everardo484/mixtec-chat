package org.mixteco.mixtecchat.di

import android.content.Context
import dagger.Module
import dagger.Provides
import org.mixteco.mixtecchat.MixtecChatApp
import org.mixteco.mixtecchat.di.qualifier.ApplicationQualifier
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: MixtecChatApp) {

    @Provides
    @Singleton
    fun provideApplication(): MixtecChatApp = app

    @Provides
    @Singleton
    @ApplicationQualifier
    fun provideApplicationContext(): Context = app
}