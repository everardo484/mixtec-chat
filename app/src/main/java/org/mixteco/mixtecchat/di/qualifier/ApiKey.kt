package org.mixteco.mixtecchat.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApiKey