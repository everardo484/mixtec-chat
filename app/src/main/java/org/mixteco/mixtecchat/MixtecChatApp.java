package org.mixteco.mixtecchat;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import org.mixteco.mixtecchat.di.ApplicationComponent;
import org.mixteco.mixtecchat.di.ApplicationModule;
import org.mixteco.mixtecchat.di.DaggerApplicationComponent;

public class MixtecChatApp extends Application {

    private static volatile Context mApplicationContext;
    public static ApplicationComponent graph;


    public static Context getAppContext() {
        return mApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mApplicationContext = getApplicationContext();

        initializeDagger();
    }

    private void initializeDagger() {
        graph =  DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
