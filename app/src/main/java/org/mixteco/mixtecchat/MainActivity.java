package org.mixteco.mixtecchat;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.mixteco.mixtecchat.common.BaseActivity;
import org.mixteco.mixtecchat.di.ApplicationComponent;

public class MainActivity extends BaseActivity<MainActivityView, MainActivityPresenter> implements MainActivityView {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return new MainActivityPresenter();
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {

    }
}
